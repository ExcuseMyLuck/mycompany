package me.eml.mycompany.commands;

import me.eml.mycompany.MyCompany;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandCompany implements CommandExecutor {
	private MyCompany mycompany;

	public CommandCompany(MyCompany mycompany) {
		this.mycompany = mycompany;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			if (cmd.getName().equalsIgnoreCase("mycompany")) {
				if (args.length < 1) {
					player.sendMessage(mycompany.chatPrefix
							+ "Usage: /mycompany help");
				}

				if (args[0].equalsIgnoreCase("create")) {
					if (!(player.hasPermission("mycompany.company.create"))) {
						player.sendMessage(mycompany.chatPrefix
								+ "You do not have permission to create a company.");
					} else {
						if (args.length == 4) {
							String companyName = args[1];
							if (mycompany.companies.isSet("Companies."
									+ companyName) == false) {
								int hourlyWage = Integer.decode(args[2]);
								int hourlyCEOWage = Integer.decode(args[3]);
								String[] ceos = {};
								String[] employes = {};

								mycompany.companies.set("Companies."
										+ companyName, null);
								mycompany.companies.set("Companies."
										+ companyName + ".owner",
										player.getName());
								mycompany.companies.set("Companies."
										+ companyName + ".ceos", ceos);
								mycompany.companies.set("Companies."
										+ companyName + ".employes", employes);
								mycompany.companies.set("Companies."
										+ companyName + ".hourlyEmployeeWage",
										hourlyWage);
								mycompany.companies.set("Companies."
										+ companyName + ".hourlyCEOWage",
										hourlyCEOWage);
								mycompany.saveConfig(mycompany.companies,
										mycompany.companiesFile);
								player.sendMessage(mycompany.chatPrefix
										+ "The company "
										+ ChatColor.RED
										+ companyName
										+ ChatColor.RESET
										+ " has been created with an hourly wage of "
										+ ChatColor.RED + hourlyWage
										+ ChatColor.RESET + ".");
							} else {
								player.sendMessage(mycompany.chatPrefix
										+ "A company with that name already exists.");
							}

						} else {
							player.sendMessage(mycompany.chatPrefix
									+ "Usage: /mycompany create <name> <hourly employe wage> <hourly CEO wage>");
						}
					}
				} else if (args[0].equalsIgnoreCase("disband")) {
					if (!(player.hasPermission("mycompany.company.disband"))) {
						player.sendMessage(mycompany.chatPrefix
								+ "You do not have permission to disband a company.");
					} else {
						if (args.length == 2) {
							String companyName = args[1];
							if (mycompany.companies.isSet("Companies."
									+ companyName) == true) {
								if (mycompany.companies.getString(
										"Companies." + companyName + ".owner")
										.equalsIgnoreCase(player.getName())) {
									mycompany.companies.set("Companies."
											+ companyName, null);
									mycompany.saveConfig(mycompany.companies,
											mycompany.companiesFile);
									player.sendMessage(mycompany.chatPrefix
											+ "You have disbanded "
											+ ChatColor.RED + companyName
											+ ChatColor.RESET + ".");
								} else {
									player.sendMessage(mycompany.chatPrefix
											+ "You must be the owner to disband a company.");
								}
							} else {
								player.sendMessage(mycompany.chatPrefix
										+ "A company with that name does not exists.");
							}

						} else {
							player.sendMessage(mycompany.chatPrefix
									+ "Usage: /mycompany disband <name>");
						}
					}
				}
			}
		} else {
			sender.sendMessage(mycompany.consolePrefix
					+ "You must be a player to use this command.");
		}
		return true;
	}
}
