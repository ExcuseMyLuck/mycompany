package me.eml.mycompany;

import java.io.File;

import me.eml.mycompany.commands.CommandCompany;
import me.eml.mycompany.util.LogManager;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

public class MyCompany extends JavaPlugin {
	private LogManager logManager = new LogManager(this);
	public String chatPrefix = ChatColor.BLUE + "[MyCompany] "
			+ ChatColor.RESET;
	public String consolePrefix = "[MyCompany] ";
	public File companiesFile;
	public FileConfiguration companies;
	public File readmeFile;
	public FileConfiguration readme;

	@Override
	public void onEnable() {
		registerEvents();
		registerCommands();
		configHandler();

		logManager.logEnable();
	}

	@Override
	public void onDisable() {
		logManager.logDisable();
	}

	private void registerEvents() {
		// Currently does nothing
	}

	private void registerCommands() {
		getCommand("mycompany").setExecutor(new CommandCompany(this));
	}

	private void configHandler() {
		companiesFile = new File(getDataFolder(), "companies.yml");
		companies = YamlConfiguration.loadConfiguration(companiesFile);

		saveConfig(companies, companiesFile);
	}

	public void saveConfig(FileConfiguration config, File file) {
		try {
			config.save(file);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void generateReadMeFile() {
		readmeFile = new File(getDataFolder(), "README.txt");
		readme = YamlConfiguration.loadConfiguration(readmeFile);

		readme.addDefault("Permissions.Node_1.Node", "mycompany.base");
		readme.addDefault("Permissions.Node_1.Description",
				"This permission allows a user to use the '/mycompany' command.");
		readme.addDefault("Permissions.Node_2.Node", "mycompany.company.create");
		readme.addDefault("Permissions.Node_2.Description",
				"This permission allows a user to use the '/mycompany create' command.");
		readme.addDefault("Permissions.Node_3.Node", "mycompany.company.disband");
		readme.addDefault("Permissions.Node_3.Description",
				"This permission allows a user to use the '/mycompany disband' command.");
	}
}
