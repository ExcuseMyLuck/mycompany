package me.eml.mycompany.util;

import java.util.logging.Level;
import java.util.logging.Logger;

import me.eml.mycompany.MyCompany;

import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginDescriptionFile;

public class LogManager {
	public static Logger log = Bukkit.getLogger();
	public PluginDescriptionFile pdFile;

	public LogManager(MyCompany mycompany) {
		pdFile = mycompany.getDescription();
	}

	public void logInfo(String logMessage) {
		log.info(logMessage);
	}

	public void logWarning(String logMessage) {
		log.log(Level.WARNING, logMessage);
	}

	public void logSevere(String logMessage) {
		log.log(Level.SEVERE, logMessage);
	}

	public void logEnable() {
		log.info(pdFile.getName() + " by " + pdFile.getAuthors()
				+ " has been enabled.");
	}

	public void logDisable() {
		log.info(pdFile.getName() + " by " + pdFile.getAuthors()
				+ " has been disabled.");
	}
}
